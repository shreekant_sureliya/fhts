// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt

var count=0;
var btns = ["qc_1", "qc_2"];
var visible=[];
var qc_task_list=[];

frappe.ui.form.on('SOP Master', {

	before_save: function(frm) {
		frappe.show_alert("qc saved");
		frappe.db.insert(qc_task_list[0]);
	},
	refresh: function(frm){
		frappe.show_alert("refresh function");
		var temp=frappe.get_children(frm.doctype,frm.docname,"add_task")
		for (var i = 0; i < temp.length; i++) {
			btns.temp[i]
		}
	}

});

frappe.ui.form.on('Task Master', {
	// refresh: function(frm) {

	// }
	
	form_render: function(frm,cdt,cdn) {
		
		// $(frm.fields_dict["add_task"].grid.grid_rows_by_docname[cdn].grid_form.fields_dict.qc_field.wrapper).html(frappe.render_template("qc"));
		// cur_frm.fields_dict["add_task"].grid.get_field('btn_1')
		
		var x=document.getElementsByClassName("btn btn-xs btn-default");
		x[0].style.background="var(--primary-color)";
		x[0].style.width="900px";
		x[0].style.color="white";
	
		x[1].style.background="var(--primary-color)";
		x[1].style.width="900px";
		x[1].style.color="white";
	
		frm.fields_dict["add_task"].grid.set_column_disp('qc_1',false);
		frm.fields_dict["add_task"].grid.set_column_disp('qc_2',false);
		

		// cur_frm.fields_dict["add_task"].grid.get_field('qc_1');
		// var child = locals[cdt][cdn];
		// var d = locals[cdt][cdn]
		// wrapper = frm.fields_dict[d.parentfield].grid.grid_rows_by_docname[cdn].grid_form.fields_dict['qc_field'].$wrapper
		// $("<div>Loading...</div>").appendTo(wrapper);
		// console.log(child.qc_1);
		// child.qc.$wrapper.html(frappe.render_template("qc"));
        
	},

	add_qc: function(frm,cdt,cdn){
		var child = locals[cdt][cdn];
		// debugger;
		console.log(child.task_title);
		if((child.task_title=== undefined)||(child.task_title=="")){
			frappe.msgprint("please add task title")
		}
		else{
			cur_frm.fields_dict["add_task"].grid.set_column_disp(btns[count],true)
			visible.push(btns[count]);
			count++;	
		}
		
		
		
		
	
	// show_alert("add",b++);
		
	},

	qc_1: function(frm,cdt,cdn){

		var d = new frappe.ui.Dialog({
			title: __("QC Points"),

		    'fields': [
		        // { fieldname : 'ht',  fieldtype : 'HTML'},
		        { fieldname : 'qc_title',  		fieldtype : 'Data', 	 label: 'QC Title'},
		        { fieldname : 'qc_stage',  		fieldtype : 'Link', 	 label: 'QC Stage',options: 'qc_stage_name'},		     
		        { fieldname : 'tollerance', 	fieldtype : 'Int', 		 label: 'Tollerance'},
		        { fieldname : 'c_break',  		fieldtype : 'Column Break'},

		        { fieldname : 'method_statment',fieldtype : 'Small Text',label: 'Method Statment'},	
		        { fieldname : 'comments', 		fieldtype : 'Small Text',label: 'Comments'},
		        { fieldname : 's_break',  		fieldtype : 'Section Break'},
		        
		        { fieldname : 'del_btn',  			fieldtype : 'Button', 	 label: 'Delete'}
		        // { fieldname : 'today',  fieldtype : 'Date', 'default': frappe.datetime.nowdate()}
		    ],
		    primary_action: function(){
		    	var qc_title_local=cur_dialog.get_field('qc_title').value
		    	var qc_stage_local=cur_dialog.get_field('qc_stage').value
		    	var tollerance_local=cur_dialog.get_field('tollerance').value
		    	var method_statment_local=cur_dialog.get_field('method_statment').value
		    	var comments_local=cur_dialog.get_field('comments').value

		        console.log(frm.docname)
		        var child = locals[cdt][cdn];
		        console.log(child)
		        debugger;	
		        console.log(child.task_title)

		        var temp=frappe.new_doc=({				
		            doctype: 'QC Points',
				    sop: frm.doc.sop_name,
				    task:child.task_title,
				    qc_title:qc_title_local
					}); 

		        qc_task_list.push(temp);
		        
		        // frappe.db.insert(temp);




		  //       frappe.db.insert({
				//     doctype: 'QC Points',
				//     sop: frm.doc.sop_name,
				//     task:child.task_title,
				//     qc_title:qc_title_local
				    
				// }).then(function(doc) { 
				//     console.log(`${doc.doctype} ${doc.name} created on ${doc.creation}`);
				// });

		        // frappe.msgprint(qc_title);

		        d.hide();
		        // show_alert(d.get_values());
		    }
		});

		d.fields_dict.del_btn.input.onclick = function() {
		frappe.msgprint("Delete operation");
		}
		// d.fields_dict.ht.$wrapper.html(frappe.render_template('qc_dialog'));
		d.show();


	}

});

