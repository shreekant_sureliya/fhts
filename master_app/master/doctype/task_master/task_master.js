// Copyright (c) 2021, shree and contributors
// For license information, please see license.txt

frappe.ui.form.on('Task Master', {
	// refresh: function(frm) {

	// }
	onload: function(frm) {
		
        frm.set_query("category_sub_type", function() {
        var doc = frm.doc
        return {
            "filters": {
                "type": doc.category_type}
                };
            }
        );
	}

});

frappe.ui.form.on('QC Master', {
    form_render: function(frm,cdt,cdn) {
	var x = document.getElementsByClassName("panel-title");
	x[x.length-1].innerHTML = cdt;
	}
})