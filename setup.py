# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in master_app/__init__.py
from master_app import __version__ as version

setup(
	name='master_app',
	version=version,
	description='master app for fhts ',
	author='shree',
	author_email='fhts@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
